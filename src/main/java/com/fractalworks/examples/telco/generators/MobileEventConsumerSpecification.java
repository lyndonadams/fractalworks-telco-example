/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.examples.telco.generators;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.transport.Transport;

/**
 * Simulated mobile device event generator consumer specification.
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "mobileEventGenerator")
public class MobileEventConsumerSpecification extends AbstractTransportSpecification {

    private boolean continuous = true;
    private long eventCount = 10000L;
    private int minsteps = 1;
    private int maxsteps = 10;
    private String trip;

    public MobileEventConsumerSpecification() {
    }

    public MobileEventConsumerSpecification(String name) {
        super(name);
    }

    public boolean isContinuous() {
        return continuous;
    }

    @JsonProperty(value = "continuous")
    public void setContinuous(boolean continuous) {
        this.continuous = continuous;
    }

    public long getEventCount() {
        return eventCount;
    }

    @JsonProperty(value = "eventCount")
    public void setEventCount(long eventCount) {
        this.eventCount = eventCount;
    }

    public int getMinsteps() {
        return minsteps;
    }

    @JsonProperty(value = "minsteps")
    public void setMinsteps(int minsteps) {
        this.minsteps = minsteps;
    }

    public int getMaxsteps() {
        return maxsteps;
    }

    @JsonProperty(value = "maxsteps")
    public void setMaxsteps(int maxsteps) {
        this.maxsteps = maxsteps;
    }

    public String getTrip() {
        return trip;
    }

    @JsonProperty(value = "trip")
    public void setTrip(String trip) {
        this.trip = trip;
    }

    @Override
    public Class<? extends Transport> getComponentClass() {
        return MobileEventConsumer.class;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if( !continuous && eventCount < 10)
            throw new InvalidSpecificationException("Cannot have less than 10 events for a non continuous consumer");

        if( trip == null || trip.isBlank() )
            throw new InvalidSpecificationException("Trip cannot be blank or null");
    }
}
