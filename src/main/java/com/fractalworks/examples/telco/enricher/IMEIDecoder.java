/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.examples.telco.enricher;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.sdk.referencedata.FieldTokenizer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Decode a IMEI string to unique parts
 *
 * Format: AA-BBBBBB-CCCCCC-D
 * Parts:
 *  TAC: AA-BBBBBB
 *  SERIAL: CCCCCC
 *  D: CHECK DIGIT
 *
 * Ref: https://www.makeuseof.com/tag/phones-imei-makeuseof-explains/
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "IMEIDecoder")
public class IMEIDecoder implements FieldTokenizer {

    public IMEIDecoder() {}

    public Optional<Map<String, Object>> decode(Object value){
        if( value instanceof String) {
            String imei = (String)value;
            String[] tokens = imei.split("-");
            if (tokens == null || tokens.length != 4) {
                return Optional.empty();
            }
            Map<String, Object> map = new HashMap<>();
            map.put("tac", tokens[0] + tokens[1]);
            map.put("deviceserial", tokens[2]);
            return Optional.of(map);
        }
        return Optional.empty();
    }
}
