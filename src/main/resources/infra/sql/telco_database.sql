
DROP DATABASE IF EXISTS telco;
CREATE DATABASE telco
    WITH
    OWNER = joule
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

\c telco;

CREATE TABLE voiceDataBundleType (
    bundleTypeId        SMALLINT PRIMARY KEY     NOT NULL,
    voiceDataType       CHAR(10)
);

INSERT INTO voiceDataBundleType(bundleTypeId, voiceDataType)
VALUES (1, 'VOICE'), (2, 'SMS'), (3, 'DATA');

CREATE TABLE bundles (
    bundleId            SMALLINT PRIMARY KEY     NOT NULL,
    bundleTypeId        SMALLINT,
    allowance           INTEGER,
    charge              MONEY,
    active              BOOLEAN
);

CREATE TABLE billing (
    billingId       SMALLINT PRIMARY KEY NOT NULL,
    imsi            CHAR(10),
    isActive        BOOLEAN,
    cardNumber      CHAR(16),
    expireDate      CHAR(4)
);

CREATE TABLE contract (
       contractId           SMALLINT PRIMARY KEY NOT NULL,
       imsi                 CHAR(10),
       startContractDate    DATE,
       billingDay           SMALLINT,
       contractTerm         SMALLINT,
       bundleId             SMALLINT REFERENCES bundles (bundleId),
       billingId            SMALLINT REFERENCES billing (billingId)
);

CREATE TABLE addresses (
   addressId    SMALLINT PRIMARY KEY NOT NULL,
   housenumber  SMALLINT,
   housename    CHAR(50),
   firstline    CHAR(50),
   secondline   CHAR(50),
   town         CHAR(30),
   postcode     CHAR(10)
);

CREATE TABLE customer (
    customerId  SMALLINT PRIMARY KEY NOT NULL,
    firstname   CHAR(20),
    surname     CHAR(30),
    dob         DATE,
    addressId   SMALLINT REFERENCES addresses (addressId),
    contractId  SMALLINT REFERENCES contract (contractId)
);

CREATE TABLE mobiledevices (
    tac  char(8) PRIMARY KEY NOT NULL,
    manufacturer   CHAR(20),
    model     CHAR(80),
    internalmodelnumber CHAR(80)
);

CREATE TABLE londonTransportStations (
    id          SMALLINT PRIMARY KEY NOT NULL,
    name        CHAR(50),
    easting     integer,
    northing    integer,
    lines       TEXT,
    network     varchar(100),
    zone        SMALLINT,
    longitude   real,
    latitude    real
);

CREATE TABLE ukPostCodeLocations (
    id          SMALLINT PRIMARY KEY NOT NULL,
    name        CHAR(10),
    latitude    real,
    longitude   real
);

CREATE TABLE uktowns (
    id              SMALLINT PRIMARY KEY NOT NULL,
    place_name      CHAR(30),
    county          CHAR(40),
    country         CHAR(20),
    grid_reference  CHAR(10),
    latitude        real,
    longitude       real,
    postcodeArea    CHAR(10)
);


\COPY addresses(addressId, housenumber, housename, firstline, secondline, town, postcode) FROM './data/addresses.csv' DELIMITER ',' CSV HEADER;
\COPY billing FROM './sql/data/billingdetails.csv' DELIMITER ',' CSV HEADER;
\COPY bundles FROM './sql/data/bundles.csv' DELIMITER ',' CSV HEADER;
\COPY contract FROM './sql/data/mobileContracts.csv' DELIMITER ',' CSV HEADER;
\COPY customer FROM './sql/data/customers.csv' DELIMITER ',' CSV HEADER;
\COPY mobiledevices FROM './sql/data/tac.csv' DELIMITER ',' CSV HEADER;
\COPY londonTransportStations FROM './sql/data/londonTubeStations.csv' DELIMITER ',' CSV HEADER;
\COPY ukPostCodeLocations FROM './sql/data/ukpostcodes.csv' DELIMITER ',' CSV HEADER;
\COPY uktowns FROM './sql/data/uk-towns.csv' DELIMITER ',' CSV HEADER;