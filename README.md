<div align="center">
  <h1>Joule Telco Demos</h1>
</div>
<div align="center">
  <strong>Modern use case platform</strong>
</div>
<div align="center">
  A framework for creating streaming applications
</div>

<div align="center">
  <h3>
    <a href="https://www.fractalworks.io/joule-platform">
      Website
    </a>
    <span> | </span>
    <a href="https://hub.docker.com/r/fractalworks/joule">
      DockerHub
    </a>
    <span> | </span>
    <a href="https://fractalworks.slack.com/archives/C021AM4M142">
      Slack
    </a>
  </h3>
</div>

## Table of Contents
- [Overview](#overview)
- [Components](#components)
- [Event Simulator](#event-simulator)
- [Examples](#examples)
    - [Opt-Out Filtering](#opt-out-filtering)
    - [Reference Data Enrichment](#reference-data-enrichment)
    - [PII Anonymisation](#anonymisation)
    - [Basic analytics](#basic-analytics)
    - [Geospatial Triggers](#geospatial-location-alerting)
    - [Geospatial Marketing](#Geospatial Marketing)
- [Starting an example](#starting-an-example)
- [Installation](#installation)
  - [Docker](#docker)
  - [Postgres](#postgres)
  - [InfluxDB](#influxdb)
  - [Apache Geode](#apache-geode)
- [Troubleshooting](#troubleshooting)
- [Support](#support)


## Overview
The objective of this project is provide a set of examples that demonstrates some of the core Joule platform capabilities.
Each use case builds upon the previous to represent how use cases can be build and tested incremetnally with the final use case combining all functionality to
demonstrate a simple direct telco marketing campaign using simulated streaming mobile events and geospatial analytics.

A set of use case examples have been defined as a sequence to gain how incremental functionality is developed for a target use case. 
We start from the basic filtering of user events to direct marketing using real-time geospatial triggers. All use cases source event data from a basic mobile event simulator, described further in the document.

1. IMSI Opt-out filtering
2. Enrich events with device information 
3. Anonymise imsi field 
4. Trigger location tracking using predefined geofences
5. Generate geospatial marketing messages based upon location

Each use case will be described in depth in upcoming FractalWorks blog posts. 

## Components
The following Joule platform components are used 

### Processors
* Filtering
* Anonymisation
* Geospatial
* Joule SDK for custom plugins

### Integrations
* Geode, caching layer to provide low latency data to processing clients, integrated to Postgres
* Postgres, database holding reference data and hosted with in a Docker container
* InfluxDB, time series database for processed events

### Joule Image
The latest Joule image can be pulled from the following [docker hub link](https://hub.docker.com/r/fractalworks/joule)

## Event Simulator
Events for these examples are generated using a simulator, events are modelled on simple mobile events. The simulator has been developed using 
the Joule SDK by extending the ```AbstractConsumerTransport``` class. 

Events are generated for five IMSI's to simulate entities moving through London streets. The default journey provided has IMSI's travelling from Waterloo station to St Pauls and back until the example process is stopped
Each IMSI speed and direction is dynamically set on each movement iteration to simulate general movement of entities in the system. Along with entity movement
the algorithm sets the corresponding connected celltower, geospatial coordinates, data usage and dropped calls. 

### Event Structure
The following fields are available on each event

```
- ingestTime
- eventTime
- imsi
- imei
- bundleid
- bytesUp
- bytesDown
- epsAttachAttempts
- smsMo2gFailRate
- droppedCall
- latlng
- celltower
```

#### Note
You can provide your own journey path, see the below example, and geofences, see further down in the document
```bash
cat conf/sources/mobileSimulatedLondonTubeTripStream.yaml
```
## Examples

## Opt-out Filtering
Within sensitive processing environments users may opt-out of personal data processing, for example real-time marketing or cross site tracking. To address this requirement event filtering is applied at the first stage of processing, known as opt-out.

Joule has an OOTB filtering processor which applies a custom Javascript expression to perform the filtering. This example simply filters a single IMSI from the processing pipeline.


```bash
File: app-filtering.env

SOURCEFILE=${PWD}/conf/sources/mobileSimulatedStream.yaml
ENGINEFILE=${PWD}/conf/usecases/filtering/mobileEventFiltering.yaml
PUBLISHFILE=${PWD}/conf/publishers/filteredMobileEventsFile.yaml
```

### Processor configuration
```yaml
- filter:
    expression: "(imsi !== '310120265624299' ) ? true : false;"
```

## Reference Data Enrichment
Often real-time streaming processing events require additional data to support further processing functions. Joule provides a solution where events are enriched in place using locally cached reference data. 

Reference data is co-located, in-memory, within the same process as Joule. This improves processing throughput by removing the need to retrieve data from traditional data stores which are typically out of process. 

A key feature of the enricher processor is the ability to load reference data on startup from connected distributed Geode data clusters thus reducing the I/O overhead on cache misses, this feature is generally known as Get Initial Image.

For this example mobile events are enriched with device information using the ```TAC``` code as the reference data lookup attribute, the ```IMEI``` value combines both the TAC and device serial code. 
The ```IMEI``` value is split into the required component parts using a user defined plugin, see class ```IMEIDecoder``` for more information. 
The resulting ```TAC``` is used to perform the reference data lookup on the linked data store, Apache Geode in this example, and binds the returned value to the event.

The 'REFERENCEDATA' variable defines the platform reference data configuration which can contain many external data stores, currently only Apache Geode is supported.

```bash
File: app-enrichment.env

SOURCEFILE=${PWD}/conf/sources/mobileSimulatedStream.yaml
REFERENCEDATA=${PWD}/conf/sources/mobileReferenceData.yaml
ENGINEFILE=${PWD}/conf/usecases/enrichment/mobileEventEnrichmentProcessWithSelect.yaml
PUBLISHFILE=${PWD}/conf/publishers/enrichedMobileEventFile.yaml
```

### Processor configuration
```yaml
- tokenizerEnricher:
   tokenizers:
    imei : com.fractalworks.examples.telco.enricher.IMEIDecoder

- enricher:
   enrich:
    deviceType:
     key: device
     using: deviceStore

   stores:
    deviceStore:
     storeName: mobiledevices
     primaryKey: tac
     primaryKeyType: java.lang.String
```


## PII Anonymisation
This example performs field level anonymisation on the customer IMSI using RSA encryption. Encryption keys are dynamically generated from the provided salt and stored in a user defined directory. Further documentation on this processor will be provided.

```bash
File: app-anonymisation.env

SOURCEFILE=${PWD}/conf/sources/mobileSimulatedStream.yaml
ENGINEFILE=${PWD}/conf/usecases/anonymisation/mobileEventAnonymisation.yaml
PUBLISHFILE=${PWD}/conf/publishers/anonymisedMobileEventFile.yaml
```

### Processor configuration
```yaml
- obfuscation:
   name: piiMasking
   cloneEvent: false
   fields:
    imsi:
     rsaEncryption:
      salt: 54ad934b14a6b0b8
      decrypt: false
      keyLocation: ./keytmp
      keyLength: 2048
      refreshKeys: true
```


## Basic analytics
This example performs a simple arithmetic function within a select statement. The resulting data is published to an influxdb event bucket, s1mme. 

You will need to create a user, organisation and a bucket within the InfluxDB Web UI, ```https://localhost:8086``` for the use case to work

### Setting
* user: joule
* organisation: joule
* bucket: s1mme

Replace the authToken and organisation values in the ```mobileEventBasicAnalyticsFunctions.yaml``` file. These values are found in the Influx UI by:
* organisation -> Setting + About
* authToken    -> Data + Tokens + Joules Token

```bash
File: app-analytics.env

SOURCEFILE=${PWD}/conf/sources/mobileSimulatedStream.yaml
REFERENCEDATA=${PWD}/conf/sources/mobileReferenceData.yaml
ENGINEFILE=${PWD}/conf/usecases/analytics/mobileEventBasicAnalyticsFunctions.yaml
PUBLISHFILE=${PWD}/conf/publishers/enrichedEventsInfluxdb.yaml
```

Explore your data with the UI, Data Explorer, by selecting s1mme-view in the first filter and then choose which data to view.


### Select configuration
```yaml
select:
   expression: "imsi, device.manufacturer, device.model, bytesUp, bytesDown, 'byteRatio' bytesUp / bytesDown, celltower, droppedCall, latitude, longitude"
```

### Transport configuration
```yaml
- influxdb:
   url: http://localhost:8086
   authToken: R-yvzhq40ruQTueWFc3euAY5v3LyPjnxC5i5FvxjQmMMNheEpK50cu-Nt5mpElwf7Qzx4hpV863ZXLMc6w32Ig==
   organisation: 6ca0390b343c00ae
   bucket: s1mme
   retentionTime: 10800
   enableGzip: true
   logLevel: BASIC
   formatter:
    useEventTime: true
    tags:
     - imsi
     - device.manufacturer
     - device.model
    measurements:
     bytesUp: DOUBLE
     bytesDown: DOUBLE
     byteRatio: DOUBLE
```

## Geospatial Triggers
This use case demonstrates geospatial location alerting using mobile location data and user defined geofences. When a mobile enters a geofence zone a tracking trigger is generated until the user exits the geofence. Three tracking states are generated per entity; Entered, dwelling and exitted. Geofences are manually defined within the configuration file.

```bash
File: app-geotrackedDevices.env

SOURCEFILE=${PWD}/conf/sources/mobileSimulatedStream.yaml
REFERENCEDATA=${PWD}/conf/sources/mobileReferenceData.yaml
ENGINEFILE=${PWD}/conf/usecases/analytics/mobileEventGeoTrackingAndAlertingStream.yaml
PUBLISHFILE=${PWD}/conf/publishers/anonymisedGeoTrackedMobileEventFile.yaml
```


#### Processor configuration
```yaml
- entityGeoTracker:
  name: imsiTracker
  trackingEntity: imsi
  minDwellingTime: 5
  timeUnit: SECONDS

  geofences:
    -
      id: 1000
      coordinates: ["51.4623328","-0.1759467"]
      radius: 150
    -
      id: 2000
      coordinates: ["51.5136287","-0.1137969"]
      radius: 150
    -
      id: 3000
      coordinates: ["51.51359909390081","-0.10022502910787918"]
      radius: 150
      
    - id: 4000
      coordinates: [ "51.51376937553133","-0.10087411403509025" ]
      radius: 150

    - id: 5000
      coordinates: [ "51.5139730193047","-0.1035375532265023" ]
      radius: 150
      
    - id: 6000
      coordinates: [ "51.51436695471256","-0.10715183002670553" ]
      radius: 150

    - id: 7000
      coordinates: [ "51.51375264191751","-0.11159351772462783" ]
      radius: 150
```


## Geospatial Marketing
This example simulates geospatial real-time marketing through the use of geofence triggers and custom messages. Geofences are automatically added to the processor through the use of auto reference data binding.

```bash
File: app-geospatialMarketingCampaign.env

SOURCEFILE=${PWD}/conf/sources/mobileSimulatedStream.yaml
REFERENCEDATA=${PWD}/conf/sources/mobileReferenceData.yaml
ENGINEFILE=${PWD}/conf/usecases/analytics/mobileEventDynamicCampaignStream.yaml
PUBLISHFILE=${PWD}/conf/publishers/marketingCampaignFile.yaml
```

Reference data stores can be used to prime the geofences. Change the required yaml file to test this feature in the app.env file.
```bash
ENGINEFILE=${PWD}/conf/usecases/analytics/mobileEventDynamicCampaignUsingGeofenceDataStoreStream.yaml
```

```yaml
- entityGeoTracker:
  name: TFLMessage
  trackingEntity: imsi
  geofenceRadius: 150.0f
  minDwellingTime: 2
  timeUnit: SECONDS

  stores:
    geoFenceStore:
      storeName: londonTransportStations
      getInitialImage: true
      initialImageQuery: select * from londonTransportStations
      primaryKey: id

- geoFenceOccupancyTrigger:
    name: marketingCampaign
    trackerField: geoTrackingInfo
    plugin: com.fractalworks.examples.telco.marketing.MarketingCampaignMessenger

- filter:
    expression: "(typeof campaignMsg !== 'undefined' && campaignMsg !== null) ? true : false;"
```


## Starting an example
Rename the use case app-XX.env you like to run to app.env within the conf directory. 

```bash
# Set environment vars
docker run -d --name=telcoapp \
-p 1098:1098 \
-v $PWD/conf:/app/joule/conf \
-v $PWD/userlibs:/app/joule/userlibs \
-v $PWD/logs:/app/joule/logs: \
-v $PWD/data:/app/joule/data: \
--user joule \
--env-file $PWD/conf/app.env \
fractalworks/joule:latest
```

Port 1098, -p switch, is used to monitor the Joule process using JMX tools such as VisualVM, JConsole etc.

## Installation
The following a describes how to set up a local environment mainly using docker images. I have provided two methods of starting up the environment either as standalone docker images or as a docker-compose method.

## Docker
The docker-compose methods automate the startup of a few example components.

* Postgres
* Influxdb
* Grafana

Execute the following command to pull, startup and configure Postgres, Influxdb and Grafana. 
```bash
docker-compose up -d
```

For Postgres all the required setting such as username, database setup and data loading is performed. For all the details take a look at:
```bash
sql/init.sql
```
This file can be used to setup the database if you choose not to use this method.

### Joule
You will need the latest version of the Joule image for either setup approach as containers are deployed based upon passed configurations.

Pull the latest version of the Joule platform. 
```bash
docker pull fractalworks/joule
```
The creation of the container will be described further down in the document.

## Postgres
PostgreSQL is a powerful, open source object-relational database system that uses and extends the SQL language combined with many features that safely store and scale the most complicated data workloads.

This step can be skipped if Postgres is already installed or using the docker compose method. 
```bash
docker pull postgres
```

Start a postgres instance 
```bash
docker run --name telco-postgres -e POSTGRES_PASSWORD=joule -e POSTGRES_USER=joule -e POSTGRES_DB=telco -p 5432:5432 -d postgres
-v ${PWD}/conf/sql:conf/sql
```
The default postgres user and database are created in the entrypoint with initdb. 

If you are using a local install first create the user joule with a password of joule that has the ability to create a database, tables and load data. Then run the below script
```bash
psql -h 127.0.0.1 -d telco -U joule -a -f sql/telco_database.sql
```

## InfluxDB
InfluxDB is a time series database designed to handle high write and query loads.
```bash
docker pull influxdb
```
This step can be skipped if Postgres is already installed or using the docker compose method.

## Apache Geode
Apache Geode is a distributed in-memory data grid supporting caching and event computation
```bash
wget https://www.mirrorservice.org/sites/ftp.apache.org/geode/1.13.2/apache-geode-1.13.2.tgz
```

This step can be skipped if you decide not to test the real-time enrichment process. Otherwise just simply decompress apache-geode-1.13.2.tgz into a directory
and set the following environment variables in the .bashrc or similar and ensure they are available in current shell.
```bash
GEODE=/<INSTALL-LOCATION>/apache-geode-1.13.2
PATH=$GEODE/bin:$PATH

source ~/.bashrc
```

### Start up cluster
Under the geode directory cluster startup and shutdown scripts are provided. Ensure the telco database has been created before running the startup script. This script with deploy a local distributed cluster. 
A locator and data member with data regions will be created.

```bash
gfsh run --file geode/scripts/geodeClusterStartup.gfsh
```

#### Processes
* locator listening on port 10334
* Data node as a cluster member

#### Regions
* customer
* billing
* bundles
* mobiledevices
* londonTransportStations
* ukpostcodes
* uktowns

#### Deployments and configuration
A number of key Fractalworks jar files are dynamically deployed to the cluster to enable data loading from a Postgres database. Further documentation will be provided that will discuss the provided Joule features.  

### Shutdown cluster
To shut down the cluster nicely execute the command below.

```bash
cd $GEODE
gfsh run --file scripts/geodeClusterShutdown.gfsh
```


## Troubleshooting
1. What if I cannot connect to Geode locator using localhost?
The quickest way is to use your machine IP address instead of localhost in the mobileReferenceData.yaml file

Error in log
```
docker logs telcoapp

Failed to add reference data store.
org.apache.geode.cache.client.NoAvailableLocatorsException: Unable to connect to any locators in the list [HostAndPort[localhost/<unresolved>:10334]]
at org.apache.geode.cache.client.internal.AutoConnectionSourceImpl.findServer(AutoConnectionSourceImpl.java:174)
```

Fix
```bash
vi conf/sources/mobileReferenceData.yaml

locatorAddress: <Your-machine-IP-address>
```


## Support
Creating examples and a platform takes a significant amount of work. Joule is independently developed, funded and maintained by Fractalworks Ltd. 
If you would like to support please contact [Fractalworks enquiries](mailto:enquiries@fractalworks.io)
